import { PDFNet } from '@pdftron/pdfnet-node';
import { BeCLog, BeCLogger, deserialize, serialize, StorageItem, StorageMetadata } from '@vid/core';

import * as internalClient from 'deployd/lib/internal-client.js';
import * as Resource from 'deployd/lib/resource.js';

import * as formidable from 'formidable';
import { Fields, Files } from 'formidable';
import * as fs from 'fs';
import { clone } from 'lodash';
import * as mime from 'mime';
import * as getDocumentProperties from 'office-document-properties';
import * as path from 'path';
import * as sharp from 'sharp';

let env;
if ( (process as any).server && (process as any).server.options && (process as any).server.options.env )
{
  env = (process as any).server.options.env;
}
else
{
  env = null;
}

let publicDir = '../../../../public';

export interface Config
{
  dbPathPrefixes: {
    contents?: string, devices?: string, deviceinterfaces?: string, i18n?: string,
    pages?: string, projects?: string, stations?: string, files?: string,
    templates?: string, users?: string, versions?: string, versiondescriptors?: string
  };
  mandant: string;
}

export class FileUploader extends Resource
{

  public logger: BeCLogger = BeCLog.createClogger( 'vid:fileuploader', 'FileUploader' );

  public static label = 'File-Upload';

  public static events = [ 'get', 'upload', 'delete' ];

  public events;

  public dpd: any;

  public static clientGeneration = true;

  public static basicDashboard = {
    settings: [
      {
        name: 'directory',
        type: 'text',
        description: 'Directory to save the uploaded files. Defaults to \'upload\'.'
      }, {
        name: 'mandant',
        type: 'text',
        description: 'The Mandant f.E. "henkel"'
      }, {
        name: 'storeName',
        type: 'text',
        description: 'the name of the files collection f.E files or cmsfiles"'
      }, {
        name: 'authorization',
        type: 'checkbox',
        description: 'Do you require user to be logged-in to view / upload / delete files?'
      }, {
        name: 'uniqueFilename',
        type: 'checkbox',
        description: 'Allow unique file name?'
      }, {
        name: 'backupExistingFiles',
        type: 'checkbox',
        description: 'Backup existing Files?'
      }
    ]
  };

  public clientGeneration = true;

  public config;

  public name;

  public session;

  public store;

  public static sanitize = ( val ) => {
    return val.replace( /[^a-zA-Z0-9-_]/g, '' );
  };

  constructor( options ) {
    super( options );

    Resource.apply( this, arguments );

    if ( env )
    {
      const dirToCheck = publicDir + '-' + env, publicDirExists = fs.existsSync( __dirname + dirToCheck );
      if ( publicDirExists )
      {
        publicDir = dirToCheck;
      }
    }

    this.config = {
      directory: this.config.directory || 'upload',
      mandant: this.config.mandant,
      storeName: this.config.storeName || 'files',
      fullDirectory: path.join( __dirname, publicDir, this.config.directory || 'upload' ),
      authorization: this.config.authorization || false,
      uniqueFilename: this.config.uniqueFilename || false,
      backupExistingFiles: this.config.backupExistingFiles || false
    };

    if ( this.name === this.config.directory )
    {
      this.config.directory = this.config.directory + '_';
    }

    // this.store = (process as any).server.createStore( this.name + 'fileuploader' );
    this.store = (process as any).server.createStore( this.config.storeName );

    // If the directory doesn't exists, we'll create it
    try
    {
      fs.statSync( this.config.fullDirectory )
        .isDirectory();
    }
    catch ( er )
    {
      fs.mkdir( this.config.fullDirectory, e => this.logger.error( 'mkdir error %O', e ) );
    }
  }

  /**
   * Handle requests
   * @param ctx
   * @param next
   */
  public handle( ctx, next ) {

    // Will send the response if all files have been processed
    const processDone = ( err = void 0 ) => {
      if ( err )
      {
        return ctx.done( err );
      }
      remainingFile--;
      if ( remainingFile === 0 )
      {
        this.logger.info( 'Response sent: ', resultFiles );
        return ctx.done( null, resultFiles );
      }
    };

    const getStorageItem = ( file, _storedProperties ) => {
      const storedObject: StorageItem = deserialize( StorageItem, clone( _storedProperties ) );
      storedObject.createdDate = new Date();
      if ( _storedProperties.uploaderId && _storedProperties.uploaderId !== '' )
      {
        storedObject.createdBy = _storedProperties.uploaderId;
      }
      storedObject.mandant = config.mandant;
      storedObject.name = file.name;
      storedObject.uploaderResourceName = self.name;

      const storedMeta: StorageMetadata = deserialize( StorageMetadata, {} );
      storedMeta.subdir = subdir;
      storedMeta.originalFilename = file.originalFilename;
      storedMeta.size = file.size;
      storedMeta.contentType = mime.getType( file.name );
      storedObject.metadata = storedMeta;
      if ( storedObject.id )
      {
        delete storedObject.id;
      }

      return storedObject;
    };

    const documentProperties = ( filePath ): Promise<any> => {
      return new Promise( ( resolve, reject ) => {
        getDocumentProperties.fromFilePath( filePath, ( err, data ) => {
          if ( err )
          {
            reject( err );
          }
          else
          {
            resolve( data );
          }
        } );
      } );
    };

    const pptThumbnails = ( _uploadDir: string, filename: string, slides: number ): Promise<string[]> => {

      const inputPath = path.resolve( _uploadDir, filename );

      const pagesFolder = filename + '_pages';
      const outputPath = path.resolve( _uploadDir, pagesFolder );
      if ( !fs.existsSync( outputPath ) )
      {
        fs.mkdirSync( outputPath );
      }

      const pages = [];

      /*for ( let i = 0; i < slides; i++ ) {
       pages.push( config.directory + '/ppt-dummy-picture.jpg' );
       }*/

      // return Promise.resolve( pages );

      const outputPathPDF = path.resolve( outputPath, `${filename}.pdf` );

      const createPDF = async () => {
        const pdfdoc = await PDFNet.PDFDoc.create();
        await pdfdoc.initSecurityHandler();
        await PDFNet.Convert.toPdf( pdfdoc, inputPath );
        await pdfdoc.save( outputPathPDF, PDFNet.SDFDoc.SaveOptions.e_linearized );
      };

      const createThumbs = async () => {

        await createPDF();

        const doc = await PDFNet.PDFDoc.createFromFilePath( outputPathPDF );
        await doc.initSecurityHandler();
        const pdfdraw = await PDFNet.PDFDraw.create( 92 );
        const itr = await doc.getPageIterator();
        let pageNumber = 1;

        while ( await itr.hasNext() )
        { //  Read every page
          const _currPage = await itr.current();
          const pageFilename = `${filename}_page_${pageNumber}.jpg`;
          const outputPathPage = path.resolve( outputPath, pageFilename );
          // console.log( 'Page', outputPathPage );
          if ( subdir && subdir !== '' )
          {
            pages.push( config.directory + '/' + subdir + '/' + pagesFolder + '/' + pageFilename );
          }
          else
          {
            pages.push( config.directory + '/' + pagesFolder + '/' + pageFilename );
          }

          await pdfdraw.export( _currPage, outputPathPage, 'JPG' );
          pageNumber++;
          await itr.next();
        }

      };

      const lic = 'demo:1681739208077:7df325750300000000c1d7f10ead5d63dd5561c61df5e5229b831dac08';

      return PDFNet.runWithCleanup( createThumbs, lic ) // you can add the key to PDFNet.runWithCleanup(main, process.env.PDFTRONKEY)
                   .then( () => {
                     PDFNet.shutdown();
                     return pages;
                   } )
                   .catch( e => {
                     return pages;
                   } );
    };

    const resizeImage = ( file, filePath: string, outPath: string, maxWidth: number, maxHeight: number ): Promise<any> => {
      const image = sharp( filePath );
      return image
        .metadata()
        .then( ( metadata ) => {
          file.width = metadata.width;
          file.height = metadata.height;
          if ( (maxWidth || maxHeight) && (metadata.width > maxWidth || metadata.height > maxHeight) )
          {
            if ( maxWidth && maxHeight )
            {
              return image
                .resize( maxWidth, maxHeight, {
                  fit: 'inside'
                } )
                .toFile( outPath )
                .then( () => {
                  file.path = outPath;
                  return sharp( outPath )
                    .metadata()
                    .then( ( resizedMetadata ) => {
                      file.width = resizedMetadata.width;
                      file.height = resizedMetadata.height;
                      return file;
                    } );
                } )
                .catch( err => {
                  return file;
                } );
            }
            else if ( maxWidth )
            {
              return image
                .resize( { width: maxWidth } )
                .toFile( outPath )
                .then( () => {
                  file.path = outPath;
                  return sharp( outPath )
                    .metadata()
                    .then( ( resizedMetadata ) => {
                      file.width = resizedMetadata.width;
                      file.height = resizedMetadata.height;
                      return file;
                    } );
                } )
                .catch( err => {
                  return file;
                } );
            }
            else if ( maxHeight )
            {
              return image
                .resize( { height: maxHeight } )
                .toFile( outPath )
                .then( () => {
                  file.path = outPath;
                  return sharp( outPath )
                    .metadata()
                    .then( ( resizedMetadata ) => {
                      file.width = resizedMetadata.width;
                      file.height = resizedMetadata.height;
                      return file;
                    } );
                } )
                .catch( err => {
                  return file;
                } );
            }
            else
            {
              return Promise.resolve( file );
            }
          }
          else
          {
            return Promise.resolve( file );
          }
        } )
        .catch( e => {
          return Promise.resolve( file );
        } );
    };

    const renameAndStore = async ( file, _storedProperties ) => {

      if ( config.backupExistingFiles === true )
      {
        // backup existing  file
        try
        {
          const exits = fs.statSync( path.join( uploadDir, file.name ) )
                          .isFile();
          if ( exits )
          {
            fs.rename( path.join( uploadDir, file.name ), path.join( uploadDir, file.name + '.' + Date.now() + '.bak' ), ( err ) => {
              if ( err )
              {
                console.error( err );
              }
            } );
          }
        }
        catch ( er )
        {
          // console.log( 'File does not exist' );
        }
      }

      const ext = path.parse( file.name )
                      .ext
                      .substring( 1 )
                      .toLowerCase();

      if ( ext === 'jpeg' || ext === 'jpg' || ext === 'png' )
      {
        file = await resizeImage( file, file.path, path.join( uploadDir, 'resized-' + file.name ), maxWidth, maxHeight );
      }

      fs.rename( file.path, path.join( uploadDir, file.name ), async ( err ) => {
        if ( err )
        {
          return processDone( err );
        }
        this.logger.info( 'File renamed after event.upload.run: %j', err || path.join( uploadDir, file.name ) );

        const storedObject: StorageItem = getStorageItem( file, _storedProperties );

        if ( file.width )
        {
          storedObject.metadata.width = file.width;
        }
        if ( file.height )
        {
          storedObject.metadata.height = file.height;
        }

        storedObject.metadata.ext = ext;

        if ( ext === 'pptx' || ext === 'ppt' )
        {
          // get number of slides
          const data = await documentProperties( path.join( uploadDir, file.name ) );
          storedObject.metadata.slides = parseInt( data.slides ) - parseInt( data.hiddenslides );
          // get page Thumbs
          storedObject.metadata.pptPageThumbs = await pptThumbnails( uploadDir, file.name, storedObject.metadata.slides );
        }

        self.store.insert( serialize( storedObject ), ( err, result ) => {
          if ( err )
          {
            return processDone( err );
          }
          this.logger.info( 'stored after event.upload.run %j', err || result || 'none' );
          const cloneResult = clone( result );
          resultFiles.push( cloneResult );
          processDone();
        } );
      } );
    };

    const newGuid = () => {
      return 'xxxxxxxxxxxx'.replace( /[xy]/g, ( c ) => {
        const r = (Math.random() * 16) | 0, v = c === 'x' ? r : (r & 0x3) | 0x8;
        return v.toString( 16 );
      } );
    };

    // session um einen client zu erzeugen
    if ( !ctx.debug )
    {
      if ( ctx.session )
      {
        this.dpd = internalClient.build( (process as any).server, ctx.session, null, ctx );
      }
      else
      {
        this.dpd = internalClient.build( (process as any).server, null, null, ctx );
      }
    }

    const req = ctx.req;
    const self = this;
    const domain: { url: any; data?: any } = { url: ctx.url };
    const me = ctx.session.user;

    const config = this.config;

    let uploadDir = config.fullDirectory;
    let uniqueFilename = config.uniqueFilename;
    let subdir;
    let maxWidth;
    let maxHeight;

    if ( config.authorization && !me )
    {
      return ctx.done( {
                         statusCode: 403,
                         message: 'You\'re not authorized to upload / modify files.'
                       } );
    }

    const resultFiles = [];
    let remainingFile = 0;

    if ( req.method === 'POST' || req.method === 'PUT' )
    {

      const form = new formidable.IncomingForm( {
                                                  maxFileSize: 10 * 1024 * 1024 * 1024,
                                                  uploadDir: uploadDir
                                                } );

      const storedProperties: any = { uploaderId: '' };
      if ( me )
      {
        storedProperties.uploaderId = me.id;
      }

      // If we received params from the request
      if ( typeof req.query !== 'undefined' )
      {
        for ( let propertyName in req.query )
        {

          if ( req.query.hasOwnProperty( propertyName ) )
          {

            this.logger.info( 'Query param found: { %j:%j } ', propertyName, req.query[ propertyName ] );

            if ( propertyName === 'maxWidth' )
            {
              console.log( 'maxWidth found: %j', req.query[ propertyName ] );
              maxWidth = parseInt( req.query[ propertyName ] );
            }
            else if ( propertyName === 'maxHeight' )
            {
              console.log( 'maxHeight found: %j', req.query[ propertyName ] );
              maxHeight = parseInt( req.query[ propertyName ] );
            }
            else if ( propertyName === 'subdir' )
            {
              console.log( 'Subdir found: %j', req.query[ propertyName ] );
              subdir = req.query[ propertyName ];
              uploadDir = path.join( uploadDir, subdir );
              // If the sub-directory doesn't exists, we'll create it
              try
              {
                fs.statSync( uploadDir )
                  .isDirectory();
              }
              catch ( er )
              {
                fs.mkdir( uploadDir, e => this.logger.error( 'mkdir error %O', e ) );
              }
            }
            else if ( propertyName === 'uniqueFilename' )
            {
              this.logger.info( 'uniqueFilename found: %j', req.query[ propertyName ] );
              uniqueFilename = req.query[ propertyName ] === 'true';
              continue; // skip to the next param since we don't need to store this value
            }

            // Store any param in the object
            try
            {
              storedProperties[ propertyName ] = JSON.parse( req.query[ propertyName ] );
            }
            catch ( e )
            {
              storedProperties[ propertyName ] = req.query[ propertyName ];
            }
          }
        }
      }

      form.on( 'file', ( name, file ) => {
        console.log( 'File %j received', file.name );
        (file as any).originalFilename = file.name;
        const filenameArray = file.name.split( '.' );
        const filename = filenameArray[ filenameArray.length - 2 ];
        const cleanName = FileUploader.sanitize( filename );
        const ext = filenameArray[ filenameArray.length - 1 ];
        if ( uniqueFilename )
        {
          file.name = newGuid() + '-' + cleanName + '.' + ext;
        }
        else
        {
          file.name = cleanName + '.' + ext;
        }

        if ( this.events.upload )
        {

          const storedObject: StorageItem = getStorageItem( file, storedProperties );

          this.events.upload.run( ctx, serialize( storedObject ), ( err ) => {
            if ( err )
            {
              return processDone( err );
            }
            renameAndStore( file, storedProperties );
          } );
        }
        else
        {
          renameAndStore( file, storedProperties );
        }
      } );

      form.on( 'fileBegin', ( name, file ) => {
        remainingFile++;
        console.log( 'Receiving a file: %j', file.name );
      } );

      form.on( 'error', ( err ) => {
        console.error( 'Error: %j', err );
        return processDone( err );
      } );

      form.parse( req, ( err: any, fields: Fields, files: Files ) => {
        if ( err )
        {
          this.logger.error( 'parse error %O', err );
        }
      } );

      return req.resume();
    }
    else if ( req.method === 'GET' )
    {
      this.get( ctx, ( err, result ) => {
        if ( err )
        {
          return ctx.done( err );
        }
        else if ( this.events.get )
        {
          domain.data = result;
          domain[ 'this' ] = result;

          this.events.get.run( ctx, domain, ( err ) => {
            if ( err )
            {
              return ctx.done( err );
            }
            ctx.done( null, result );
          } );
        }
        else
        {
          ctx.done( err, result );
        }
      } );
    }
    else if ( req.method === 'DELETE' )
    {
      if ( FileUploader.events[ 'delete' ] )
      {
        FileUploader.events[ 'delete' ].run( ctx, domain, ( err ) => {
          if ( err )
          {
            return ctx.done( err );
          }
          self.del( ctx, next );
        } );
      }
      else
      {
        this.del( ctx, next );
      }
    }
    else
    {
      next();
    }
  }

  public get( ctx, next ) {
    const self = this;
    const id = ctx.url.split( '/' )[ 1 ];
    if ( id.length > 0 )
    {
      ctx.query.id = id;
    }

    self.store.find( ctx.query, next );
  }

  // Delete a file
  public del( ctx, next ) {

    const self = this, fileId = ctx.url.split( '/' )[ 1 ], uploadDir = this.config.fullDirectory, me = ctx.session.user;

    const deleteFolderRecursive = ( path ) => {
      if ( fs.existsSync( path ) )
      {
        fs.readdirSync( path )
          .forEach( ( file, index ) => {
            const curPath = path + '/' + file;
            if ( fs.lstatSync( curPath )
                   .isDirectory() )
            { // recurse
              deleteFolderRecursive( curPath );
            }
            else
            { // delete file
              fs.unlinkSync( curPath );
            }
          } );
        fs.rmdirSync( path );
      }
    };

    const findObj = { id: fileId };

    this.store.find( findObj, ( err, result ) => {
      if ( err )
      {
        return ctx.done( err );
      }

      const storedObject: StorageItem = deserialize( StorageItem, clone( result ) );

      this.logger.info( 'found %j', err || storedObject || 'none' );
      if ( typeof storedObject !== 'undefined' )
      {
        let subdir;
        if ( storedObject.metadata.subdir )
        {
          subdir = storedObject.metadata.subdir;
        }
        self.store.remove( { id: fileId }, ( err ) => {
          if ( err )
          {
            return ctx.done( err );
          }

          // delete storedObject.metadata.pptThumbnails && pdf as well  795be692f7dc-Henkel-Dummy-PPT.pptx_pages
          const pagesFolder = storedObject.name + '_pages';

          // in case you don't upload to a subdir
          if ( subdir )
          {
            if ( fs.existsSync( path.join( uploadDir, subdir, pagesFolder ) ) )
            {
              deleteFolderRecursive( path.join( uploadDir, subdir, pagesFolder ) );
            }
            fs.unlink( path.join( uploadDir, subdir, storedObject.name ), ( err ) => {
              if ( err )
              {
                return ctx.done( err );
              }
              ctx.done( null, {
                statusCode: 200,
                message: 'File ' + storedObject.name + ' successfuly deleted'
              } );
            } );
          }
          else
          {
            if ( fs.existsSync( path.join( uploadDir, pagesFolder ) ) )
            {
              deleteFolderRecursive( path.join( uploadDir, pagesFolder ) );
            }
            fs.unlink( path.join( uploadDir, storedObject.name ), ( err ) => {
              if ( err )
              {
                return ctx.done( err );
              }
              ctx.done( null, {
                statusCode: 200,
                message: 'File ' + storedObject.name + ' successfuly deleted'
              } );
            } );
          }
        } );
      }
    } );
  }
}

module.exports = FileUploader;
