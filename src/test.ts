import { FileUploader } from './index';

const dpd = require( 'dpd-js-sdk' )( 'http://cms.abb.local:2504' );
dpd.pages = dpd( '/pages' );
dpd.templates = dpd( '/templates' );
dpd.contents = dpd( '/contents' );
dpd.projects = dpd( '/projects' );
dpd.files = dpd( '/files' );
dpd.i18n = dpd( '/i18n' );

const test = new FileUploader( null );

test.dpd = dpd;

test.handle( {
  done: ( error, res ) => {
    if ( error ) {
      console.log( 'done:error', error );
    } else {
      console.log( 'done:res', JSON.stringify(res, null, 1) );
    }
  },
  query: {
    projectID_: 'xx',
    pageID: 'c88886932bf148ad',
    constentID_: 'xx',
    lang: 'de'
  },
  method: 'GET',
  debug: true
}, () => {

} );

